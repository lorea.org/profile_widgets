<?php

function profile_widgets_init() {

	// Set dashboard as default page after login
	if (elgg_is_active_plugin('dashboard') && !isset($_SESSION['last_forward_from'])) {
		$_SESSION['last_forward_from'] = 'dashboard';
	}

	elgg_register_widget_type('thewirewrite', elgg_echo('thewire:add'), elgg_echo("thewire:yourdesc:write:desc"), 'dashboard');
}

elgg_register_event_handler('init', 'system', 'profile_widgets_init');
